#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente que abre un socket a un servidor
"""
import sys
import socket

metodo = sys.argv[1]
try:
    usuario = sys.argv[2]
    datos = usuario.split(':')
    nombre = datos[0]
    dir_ip = nombre.split('@')[1]
    puerto = int(datos[1])
    LINE = metodo + ' sip:' + nombre + ' SIP/2.0\r\n\r\n'

except (IndexError, ValueError):
    sys.exit('Usage:client.py method receiver@IP:SIPport')
# Cliente UDP simple.

# Dirección IP del servidor.


# Contenido que vamos a enviar

LINE2 = 'v=0 \r\no= tumadre@tuhija' + dir_ip + \
        '\r\ns= tufamilia \r\nt=0 \r\nm= audio ' + str(puerto) + ' RTP'
longitud = len(LINE2)
lenght = 'Content-Lenght: ' + str(longitud) + '\r\n'
LINES = LINE + lenght + LINE2

# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    my_socket.connect((dir_ip, puerto))
    print("Enviando: " + LINE)

    if metodo != 'INVITE':
        my_socket.send(bytes(LINE, 'utf-8'))
    elif metodo == 'INVITE':
        my_socket.send(bytes(LINES, 'utf-8'))
    data = my_socket.recv(1024)

    print('Recibido -- ', data.decode('utf-8'))

    if metodo == 'INVITE':
        leer_respuesta = data.decode('utf-8')
        mensaje_cliente = 'SIP/2.0 200 OK'
        respuesta_servidor = leer_respuesta.split('\r\n')[4]

        if respuesta_servidor == mensaje_cliente:
            asentimiento = 'ACK' + ' sip:' + nombre + ' SIP/2.0\r\n\r\n'
            my_socket.send(bytes(asentimiento, 'utf-8'))


print("Terminando socket...")
print("Fin.")
