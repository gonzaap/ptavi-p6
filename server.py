#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""
import sys
import socketserver
import simplertp
import secrets


class EchoHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """

    def handle(self):
        # Escribe dirección y puerto del cliente (de tupla client_address)
        mensaje = self.rfile.read().decode('utf-8')
        print(mensaje)
        metodo = mensaje.split(' ')[0]
        if metodo in ['INVITE', 'ACK', 'BYE']:
            if mensaje.split(' ')[1].split(':')[0] != 'sip':
                self.wfile.write(b'SIP/2.0 400 Bad Request\r\n')

            else:

                if metodo == 'INVITE':
                    respuesta = b'SIP/2.0 100 Trying\r\n\r\n'
                    respuesta = (respuesta + b'SIP/2.0 180 Ringing\r\n\r\n')
                    respuesta = (respuesta + b'SIP/2.0 200 OK\r\n\r\n')
                    self.wfile.write(respuesta)

                elif metodo == 'ACK':
                    BIT = secrets.randbelow(1)
                    RTP_header = simplertp.RtpHeader()
                    RTP_header.set_header(version=2, marker=BIT,
                                          payload_type=14, ssrc=200002)
                    audio = simplertp.RtpPayloadMp3(archivo)
                    simplertp.send_rtp_packet(RTP_header, audio, dir_ip, 23032)

                if metodo == 'BYE':
                    respuesta = b'SIP/2.0 200 OK\r\n'
                    self.wfile.write(respuesta)

        else:
            self.wfile.write(b'SIP/2.0 405 Method Not Allowed\r\n')


if __name__ == "__main__":
    # Creamos servidor de eco y escuchamos
    dir_ip = sys.argv[1]
    puerto = sys.argv[2]
    archivo = sys.argv[3]
    serv = socketserver.UDPServer((dir_ip, int(puerto)), EchoHandler)
    print("Lanzando servidor UDP de eco...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        sys.exit('Finalizando el servidor')
